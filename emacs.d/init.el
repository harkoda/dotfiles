;;;;;;; MELPHA

(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  GENERAL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Save desktop on exit
(desktop-save-mode 1)

;; UTF-8 All things
(prefer-coding-system 'utf-8)

;; Push shell to current buffer
(push (cons "\\*shell\\*" display-buffer--same-window-action) display-buffer-alist)


;; Disable auto-save and auto-backup
(setq auto-save-list-file-prefix nil)
(setq auto-save-default nil)
(setq make-backup-files nil)


;; Enable word-wrap
(setq-default word-wrap t)
(global-visual-line-mode t)


;;;;;;; LOAD PACKAGES

(add-to-list 'load-path "~/.emacs.d/lisp/")
(load "org-download")


;;;;;;; REMAPPINGS

; Unbind meta for right alt key
(setq mac-option-key-is-meta t)
(setq mac-right-option-modifier nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; KEY BINDINGS 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; HELM - bind M-y to show kill ring
(global-set-key (kbd "M-y") 'helm-show-kill-ring)

;; HELM - bind C-x b to helm-mini
(global-set-key (kbd "C-x b") 'helm-mini)

;; HELM - bind C-X C-f to find files
(global-set-key (kbd "C-x C-f") 'helm-find-files)

;; HELM - bind M-i to helm-swoop
(global-set-key (kbd "M-i") 'helm-swoop)

;; HELM - bind C-x M-i to swoop all files
(global-set-key (kbd "C-x M-i") 'helm-multi-swoop-all)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  EVILMODE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; enable evil mode
(require 'evil)
  (evil-mode 1) 


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  ORG-MODE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Set export types
(setq org-export-backends (quote (ascii beamer html latex md)))

;; Display inline images
(setq org-display-inline-images t) 
(setq org-redisplay-inline-images t) 
(setq org-startup-with-inline-images "inlineimages")


;;;;;;; ORG DOWNLOAD
(setq-default org-download-image-dir "./assets")
(setq org-download-heading-lvl nil)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  CUSTOM SET VARIABLES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "red3" "ForestGreen" "yellow3" "blue" "magenta3" "DeepSkyBlue" "gray50"])
 '(custom-enabled-themes (quote (manoj-dark)))
 '(package-selected-packages (quote (fontawesome helm-swoop async))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

